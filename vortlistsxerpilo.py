import time, selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
#ac = selenium.ActionChains(driver)

driver = webdriver.Chrome()
driver.get("http://www.duolingo.com/register")

def klaku_DIV_kun_titolo(tag, teksto):
    for e in driver.find_elements_by_tag_name(tag):
        dtext = e.text
        #print(e.text, "dtext='", dtext.lower(), "' teksto= '", teksto.lower(), "'", dtext.lower() == teksto.lower())
        if dtext.lower() == teksto.lower():
            e.click()
            return True
    print("Ne puŝis ion kun '{}'... :(".format(teksto))
    return False

#lingvoelekto
klaku_DIV_kun_titolo('div', "Esperanto")
time.sleep(10)
#sen konto
button = driver.find_elements_by_tag_name("button")[-1]
driver.execute_script("arguments[0].click();", button)
time.sleep(.5)
#"learning goal"
button = driver.find_elements_by_tag_name("button")[-1]
driver.execute_script("arguments[0].click();", button)
time.sleep(.5)
#mi ne konas esperanton
button = driver.find_elements_by_tag_name("button")[-2]
driver.execute_script("arguments[0].click();", button)
time.sleep(.5)
#cxesu la teston
#driver.find_element_by_tag_name("a").click()
#iru al la vortaro
#la pli ol sescent vortojn de duolingvo, kopiite de la anki-kartojn kiel listo
esperanto_vortoj_duolingo = [v.split(";")[0] for v in open("ankivortaro.txt").readlines()]

parto=7
fr = open("malpuraj_html_frazoj_{}_.txt".format(parto), "w")
for i, v in enumerate(esperanto_vortoj_duolingo[(parto-1)*(100):(parto)*(100)]):
    try:
        driver.get("https://www.duolingo.com/dictionary/eo")
        time.sleep(1)
        input = driver.find_element_by_tag_name("input")
        time.sleep(1)
        input.send_keys(v);
        time.sleep(1)
        button = driver.find_elements_by_tag_name("button")[0]
        driver.execute_script("arguments[0].click();", button)
        #klaku por vidi cxiuj frzojn
        time.sleep(3)
        try:
            klaku_DIV_kun_titolo("span", "Show More Sentences")
        except Exception as e:
            klaku_DIV_kun_titolo("span", "Show More Sentences")
        f = driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/div/div[2]/div").text
        print(i,"/",len(esperanto_vortoj_duolingo))
        fr.write(f)
    except Exception as e:
        print("Fiaskis kun '{}'".format(v))
        pass
fr.close()


#element=driver.find_element_by_xpath("//div[2]/div/div[2]/div[1]")
#driver.find_elements_by_tag_name("button")[-1].click()
#driver.close()
